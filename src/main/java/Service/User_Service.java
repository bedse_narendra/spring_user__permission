package Service;

import java.util.List;

import Model.Student;
import Model.User;
import Model.UserRole;

public interface User_Service {
	public List<User> checkUser(User user);
	public boolean saveReg(User user);
	public List<UserRole> getRoleType();
	public boolean saveUser(User user);
	public List<User> getUsers();
//	public boolean deleteUser(User user);
//	public List<User> getUserByID(User user);
//	public boolean updateUser(User user);
}
