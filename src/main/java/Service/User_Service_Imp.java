package Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import DAO.User_DAO;
import Model.Student;
import Model.User;
import Model.UserRole;

@Service
@Transactional
public class User_Service_Imp implements User_Service {
	
	@Autowired
	private User_DAO userdao;
	
	
	@Override
	public boolean saveReg(User user) {
		return userdao.saveReg(user);
	}

	@Override
	public List<UserRole> getRoleType() {
		return userdao.getRoleType();
	}

	@Override
	public List<User> checkUser(User user) {
		return userdao.checkUser(user);
	}

	@Override
	public boolean saveUser(User user) {
		return userdao.saveUser(user);
	}

	@Override
	public List<User> getUsers() {
		return userdao.getUsers();
	}

//	@Override
//	public boolean deleteUser(User user) {
//		return userdao.deleteUser(user);
//	}
//
//	@Override
//	public List<User> getUserByID(User user) {
//		return userdao.getUserByID(user);
//	}
//
//	@Override
//	public boolean updateUser(User user) {
//		return userdao.updateUser(user);
//	}
}
