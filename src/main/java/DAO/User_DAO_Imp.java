package DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import Model.User;
import Model.UserRole;
@Repository
public class User_DAO_Imp implements User_DAO {
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public List<User> checkUser(User user) {
		List<User> list=new ArrayList<User>();
		 
		try {
			
			Session currentSession = sessionFactory.getCurrentSession();
			Query<User> query=currentSession.createQuery("from User where username= :username and password= :password and role= :role", User.class);
			query.setParameter("username", user.getUsername());
			query.setParameter("password", user.getPassword());
			query.setParameter("role", user.getRole());
		    list=query.getResultList();
		              
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Logged in check"+list);
	return list;
	}
	
	
	@Override
	public boolean saveReg(User user) {
		boolean status=false;
		try {
			sessionFactory.getCurrentSession().save(user);
			status=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}


	@Override
	public List<UserRole> getRoleType() {
		Session currentSession = sessionFactory.getCurrentSession();
	    Query query=currentSession.createQuery("select role from UserRole");//here persistent class name is Emp  
	    List list=query.list();
	    return list; 
	}


	@Override
	public boolean saveUser(User user) {
		boolean status=false;
		try {
			sessionFactory.getCurrentSession().save(user);
			status=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}


	@Override
	public List<User> getUsers() {
		Session currentSession = sessionFactory.getCurrentSession();
		 return currentSession.createQuery("SELECT a FROM User a", User.class).getResultList();  
//		List<User> list=new ArrayList<User>();
//		System.out.println("DAO ...!");
//		
//		Query<User> query=currentSession.createQuery("from User",User.class);
//		// List list=query.list();
//		//	List list=query.getResultList();
//		 list=query.getResultList();
//		System.out.println("DAO ..."+list);
//		return list;
	}


//	@Override
//	public boolean deleteUser(User user) {
//		boolean status=false;
//		try {
//			sessionFactory.getCurrentSession().delete(user);
//			status=true;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return status;
//	}
//
//
//	@Override
//	public List<User> getUserByID(User user) {
//		Session currentSession = sessionFactory.getCurrentSession();
//		Query<User> query=currentSession.createQuery("from User where user_id=:user_id", User.class);
//		query.setParameter("user_id", user.getUser_id());
//		List<User> list=query.getResultList();
//		return list;
//	}
//
//
//	@Override
//	public boolean updateUser(User user) {
//		boolean status=false;
//		try {
//			sessionFactory.getCurrentSession().update(user);
//			status=true;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return status;
//	}
}
