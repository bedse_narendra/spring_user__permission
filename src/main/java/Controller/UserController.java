package Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import Model.Student;
import Model.User;
import Model.UserRole;
import Service.Student_Service;
import Service.User_Service;

@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping(value="/api")
public class UserController {
	
	@Autowired
	private User_Service userservice;
	
	
	
	
	@PostMapping("authenticate")
	public String checkLogin(@RequestBody User user) {
		
		Gson gson = new Gson();
		
		  List<User> list=userservice.checkUser(user);
		 
		String t="";
		  
		  if(list.isEmpty())
		  {
			  System.out.println("null: ");
			  return t;
			 
		  }
		  else
		  {
			 
			  String jsonCartList = gson.toJson(list);
			
			  System.out.println("jsonCartList: " + jsonCartList);
			  return jsonCartList;
		  }	
	}

	@PostMapping("register")
	public  boolean saveReg(@RequestBody User user) {
		//System.out.println(user.getFirstName());
		 return userservice.saveReg(user);
		
	}
	
	@GetMapping("roletype")
	public List<UserRole> getRoleType() {
		
		List<UserRole> roletype=userservice.getRoleType();
		System.out.println(roletype);
		 return roletype;
		
	}
	
	
	
	@PostMapping("save-user")
	public boolean saveUser(@RequestBody User user) {
		 return userservice.saveUser(user);
		
	}
	
	@GetMapping("users-list")
	public List<User> allUsers() {
		 return userservice.getUsers();
	}
	
	
	/*
	 * @DeleteMapping("delete-user/{user_id}") public boolean
	 * deleteUser(@PathVariable("user_id") int user_id,User user) {
	 * user.setUser_id(user_id); return userservice.deleteUser(user); }
	 * 
	 * @GetMapping("user/{id}") public List<User>
	 * alluserByID(@PathVariable("user_id") int user_id,User user) {
	 * user.setUser_id(user_id); return userservice.getUserByID(user);
	 * 
	 * }
	 * 
	 * @PostMapping("update-user/{id}") public boolean updateUser(@RequestBody User
	 * user,@PathVariable("user_id") int user_id) { user.setUser_id(user_id); return
	 * userservice.updateUser(user); }
	 */
}
